# ShinyDialect



Une fois l'application lancée, afin de la tester faire :
> Menu déroulant "Gestion des données" :
> -   "Importation des données" et charger le fichier "Champignon v1-0 -Carole-26-01-201 - Der.xlsx" qui se trouve dans APP1/DATA/
> -   "Choix des données" Choisir -> Longitude : _LON_VERIF
                                              -> Latitude : _LAT_VERIF
                                              -> Variable à expliquer : _lemmeR1


> Menu déroulant "Reglages de la carte" :
> -   "Choix du contour de carte" selectionner france.csv
> -   "Méthode" -> Reglage des differents paramètre, lancer, attendre que le programme charge
